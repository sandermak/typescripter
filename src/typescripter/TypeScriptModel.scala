package typescripter

sealed trait TSDecl
case class TSClass(name: String, members: Seq[TSMember], exported: Boolean = true) extends TSDecl
case class TSEnum(name: String, members: Seq[String], exported: Boolean = true) extends TSDecl

sealed trait TSMember
case class TSField(name: String, tpe: TSType, optional: Boolean = false, initializer: Option[String] = None) extends TSMember
case class TSMethod(name: String, args: Seq[Arg], returnType: TSType) extends TSMember

case class Arg(name: String, tpe: TSType)

sealed trait TSType
case object TSString extends TSType
case object TSNumber extends TSType
case object TSBoolean extends TSType
case object TSVoid extends TSType
case object TSAny extends TSType
case class TSArray(tpe: TSType) extends TSType
case class TSClassType(name: String) extends TSType

object PrettyPrinter {

  def prettyPrint[T: Printable](input: T): String =
    implicitly[Printable[T]].print(input)

  import annotation.implicitNotFound
  @implicitNotFound("No member of type class Printable in scope for ${T}")
  trait Printable[T] {
    def print(input: T): String
  }

  implicit object PrintableStringType extends Printable[TSType] {
    def print(tpe: TSType) = tpe match {
      case TSString => "string"
      case TSNumber => "number"
      case TSBoolean => "bool"
      case TSVoid => "void"
      case TSAny => "any"
      case TSArray(tpe) => print(tpe) + "[]"
      case TSClassType(name) => name
    }
  }

  implicit object PrintableTSMember extends Printable[TSMember] {
    def print(tpe: TSMember) = tpe match {
      case TSField(name, tpe, optional, initializer) => printField(name, tpe, optional, initializer)
      case TSMethod(name, args, returnType) => ???
    }

    def printField[T <: TSType: Printable](name: String, tpe: T, optional: Boolean, initializer: Option[String]) = {
      val printer = implicitly[Printable[T]]
      val initExpr = initializer.map(" = " + _).getOrElse("")
      s"$name${if (optional) "?" else ""}: ${prettyPrint(tpe)}$initExpr"
    }

  }

  implicit object PrintableTSDecl extends Printable[TSDecl] {
    def print(tsDecl: TSDecl) = tsDecl match {
      case tsClass: TSClass =>
        s"""
${if (tsClass.exported) "export "}class ${tsClass.name} {
  ${tsClass.members.map((member: TSMember) => prettyPrint(member)).mkString(";\n  ")};
}
""" 
      case tsEnum: TSEnum =>
        s"""
${if (tsEnum.exported) "export "}enum ${tsEnum.name} {
  ${tsEnum.members.mkString(",\n  ")}
}
""" 
    }
  }

}

