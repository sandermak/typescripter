package typescripter

import org.objectweb.asm.ClassReader
import java.io.FileInputStream
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode
import org.objectweb.asm.Type
import org.objectweb.asm.signature.SignatureReader
import org.objectweb.asm.signature.SignatureVisitor

object JavaClassReader {
  import scala.collection.JavaConverters._

  val numberTypes = Set("I", "J", "F", "S", "B", "Ljava/lang/Integer;", "Ljava/lang/Long;", "Ljava/lang/Float;", "Ljava/lang/Short;", "Ljava/lang/Byte;")
  val booleanTypes = Set("Z", "Ljava/lang/Boolean;")
  val stringTypes = Set("C", "Ljava/lang/String;", "Ljava/lang/Character;")

  def read(file: String): TSDecl = {
    val reader = new ClassReader(new FileInputStream(file))
    val classname = reader.getClassName.substring(reader.getClassName.lastIndexOf("/") + 1)
    println(s"Processing $classname");
    val classNode = new ClassNode()

    reader.accept(classNode, Opcodes.ASM5)
    println(classNode.signature)
    if (classNode.signature != null && classNode.signature.startsWith("Ljava/lang/Enum")) 
      processEnum(classname, classNode)
    else 
      processClass(classname, classNode)
  }

  def processClass(classname: String, classNode: ClassNode): TSDecl = {
    val fields: Seq[TSMember] =
      for (field <- classNode.fields.asScala.asInstanceOf[Seq[FieldNode]]) yield {
        println(s"${field.name}, desc: ${field.desc}, sig: ${field.signature}");
        TSField(field.name, toTSType(field.desc, field.signature));
      }

    TSClass(classname, fields);
  }

  def processEnum(classname: String, classNode: ClassNode): TSDecl = {
     val members: Seq[String] = 
       for (field <- classNode.fields.asScala.asInstanceOf[Seq[FieldNode]] 
                     if !field.name.equals("ENUM$VALUES")) yield {
        println(s"${field.name}, desc: ${field.desc}, sig: ${field.signature}");
        field.name
     }
    TSEnum(classname, members);
  }

  // TODO more robust type handling using ASM Type?
  def toTSType(desc: String, sig: String): TSType = {
    def getType(desc: String): TSType = desc match {
      case desc if stringTypes.contains(desc) => TSString
      case desc if numberTypes.contains(desc) => TSNumber
      case desc if booleanTypes.contains(desc) => TSBoolean
      case desc if isCollection(desc) => TSArray(getElementType(desc, sig))
      case desc => TSClassType(desc.substring(desc.lastIndexOf("/") + 1).init)
    }

    def isCollection(desc: String) = desc.startsWith("[") || desc.endsWith("/Set;") || desc.endsWith("/List;") || desc.endsWith("/Collection;")
    def getElementType(desc: String, sig: String) = if (desc.startsWith("["))
      getType(desc.tail)
    else {
      import java.util.regex.Pattern;
      val matcher = Pattern.compile(".*<(.*?)>.*").matcher(sig)
      matcher.matches()
      getType(matcher.group(1))
    }

    getType(desc)
  }

}