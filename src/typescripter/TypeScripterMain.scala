package typescripter

object TypeScripterMain extends App {
  println(PrettyPrinter.prettyPrint(JavaClassReader.read("./test-input/LoggedInUser.class")))
}